---
title: "rhvdpols.gitlab.io"
date: 2019-08-08T21:34:23+02:00
weight: 2
draft: false
summary: "A statically generated web-page using Hugo (generator) and Bootstrap (styling)."
link: "https://gitlab.com/rhvdpols/rhvdpols.gitlab.io"
---

This is a more detailed description for this project. This discusses things like the tech used and the rationale for creating the project.

#### Tech
* Hugo
* Bootstrap