---
title: "sudoku"
date: 2019-08-08T21:14:03+02:00
weight: 1
draft: false
summary: "A sudoku solver in Rust. Originally in C#, created as an assignment for a job interview."
link: "https://gitlab.com/rhvdpols/sudoku"
---

This is a more detailed description for this project. This discusses things like the tech used and the rationale for creating the project.

#### Tech
* Rustup
* Cargo