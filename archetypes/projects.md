---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
weight: 10
draft: false
summary: "This is a summary"
link: "https://gitlab.com/rhvdpols/project"
---

This is a more detailed description for this project. This discusses things like the tech used and the rationale for creating the project.

#### Tech
* Stuff